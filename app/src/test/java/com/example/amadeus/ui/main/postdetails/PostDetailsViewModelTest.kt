package com.example.amadeus.ui.main.postdetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.amadeus.MainCoroutineRule
import com.example.amadeus.data.local.PostEntity
import com.example.amadeus.data.local.PostWithAuthor
import com.example.amadeus.data.local.UserEntity
import com.example.amadeus.data.repo.PostAndAuthorRepository
import com.example.amadeus.runBlocking
import com.example.amadeus.util.network.Resource
import com.nhaarman.mockitokotlin2.notNull
import com.nhaarman.mockitokotlin2.whenever

import junit.framework.TestCase.assertNotNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import org.mockito.Mockito.times
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.Spy

@ExperimentalCoroutinesApi
class PostDetailsViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: PostDetailsViewModel

    @Mock
    lateinit var repository: PostAndAuthorRepository

    @Mock
    lateinit var postDetailsObserver: Observer<in PostDetailsPresentation?>

    @Mock
    lateinit var statusObserver: Observer<in Resource.Status>

    @Before
    @ExperimentalCoroutinesApi
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun noCallToRepository_whenPostIdIsNotProvided() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            initializeMock()

            verify(repository, times(0)).refreshPostDetails(anyInt())
            verify(repository, times(0)).requestInitialPostWithAuthorData(anyInt())
        }
    }

    @Test
    fun callRepository_whenValidPostIdIsProvided() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            initializeMock()

            viewModel.setPostId(anyInt())
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(repository, times(1)).requestInitialPostWithAuthorData(anyInt())
            verify(repository, times(1)).refreshPostDetails(anyInt())
        }
    }

    @Test
    fun makeMultipleRepositoryCalls_whenDataRefreshIsRequested() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            initializeMock()

            viewModel.setPostId(anyInt())
            viewModel.refreshData()
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(repository, times(1)).requestInitialPostWithAuthorData(anyInt())
            verify(repository, times(2)).refreshPostDetails(anyInt())
        }
    }


    @Test
    fun statusIsUpdated_whenPostsAreRefreshed() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            initializeMock()

            whenever(repository.refreshPostDetails(anyInt()))
                .thenReturn(Resource.Status.SUCCESS)
                .thenReturn(Resource.Status.ERROR)

            viewModel.setPostId(anyInt())
            viewModel.refreshData()
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(statusObserver, times(1)).onChanged(Resource.Status.SUCCESS)
            verify(statusObserver, times(1)).onChanged(Resource.Status.ERROR)
        }
    }

    @Test
    fun statusChanges_withEachRefresh() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            initializeMock()

            whenever(repository.refreshPostDetails(anyInt())).thenReturn(Resource.Status.SUCCESS)

            viewModel.setPostId(anyInt())
            viewModel.refreshData()
            viewModel.refreshData()
            viewModel.refreshData()
            viewModel.refreshData()
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(statusObserver, times(5)).onChanged(notNull())
        }
    }

    @Test
    fun postDetailsAreNotNull_whenInitialized() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            initializeMock()
            viewModel.setPostId(anyInt())

            mainCoroutineRule.testDispatcher.resumeDispatcher()

            assertNotNull(viewModel.postData.value)
        }
    }

    @Test
    fun postDetailsChange_whenRepositoryEmitsNewValues() {
        mainCoroutineRule.runBlocking {
            initializeViewModel()
            whenever(repository.requestInitialPostWithAuthorData(anyInt())).thenReturn(flow {
                emit(PostWithAuthor(PostEntity(), UserEntity()))
                emit(PostWithAuthor(PostEntity(), UserEntity()))
                emit(PostWithAuthor(PostEntity(), UserEntity()))
                emit(PostWithAuthor(PostEntity(), UserEntity()))
            })

            whenever(repository.refreshPostDetails(anyInt())).thenReturn(Resource.Status.SUCCESS)

            viewModel.setPostId(anyInt())
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(postDetailsObserver, times(4)).onChanged(notNull())
        }
    }

    private fun initializeViewModel() {
        mainCoroutineRule.testDispatcher.pauseDispatcher()
        viewModel = PostDetailsViewModel(repository, mainCoroutineRule.testDispatcher)
        viewModel.apply {
            networkRequestStatus.observeForever(statusObserver)
            postData.observeForever(postDetailsObserver)
        }
    }

    private fun initializeMock() {
        whenever(repository.requestInitialPostWithAuthorData(anyInt())).thenReturn(flow {
            emit(PostWithAuthor(PostEntity(), UserEntity()))
        })
    }

}
