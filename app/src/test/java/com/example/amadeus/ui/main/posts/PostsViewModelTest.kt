package com.example.amadeus.ui.main.posts

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.amadeus.MainCoroutineRule
import com.example.amadeus.data.local.PostEntity
import com.example.amadeus.data.repo.PostRepository
import com.example.amadeus.runBlocking
import com.example.amadeus.util.network.Resource
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class PostsViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel

    @Mock
    lateinit var repository: PostRepository

    @Mock
    lateinit var postsObserver: Observer<in List<PostPresentation>?>

    @Mock
    lateinit var statusObserver: Observer<in Resource.Status>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        mainCoroutineRule.testDispatcher.pauseDispatcher()

        whenever(repository.getAllPosts()).thenReturn(flow {
            emit(listOf(PostEntity()))
            emit(listOf(PostEntity()))
            emit(listOf(PostEntity()))
        })

        viewModel = MainViewModel(repository, mainCoroutineRule.testDispatcher)

        viewModel.apply {
            networkRequestStatus.observeForever(statusObserver)
            posts.observeForever(postsObserver)
        }
    }


    @Test
    fun statusIsLoading_whenViewModelIsCreated() =
        mainCoroutineRule.runBlocking {
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(statusObserver).onChanged(Resource.Status.LOADING)
        }

    @Test
    fun necessaryNetworkCallsAreMade_whenViewModelIsCreated() =
        mainCoroutineRule.runBlocking {
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(repository, times(1)).refreshAllPosts()
            verify(repository, times(1)).getAllPosts()
        }


    @Test
    fun statusIsSuccess_whenInitialNetworkCallReturnsSuccess() =
        mainCoroutineRule.runBlocking {
            whenever(repository.refreshAllPosts()).thenReturn(Resource.Status.SUCCESS)

            mainCoroutineRule.testDispatcher.resumeDispatcher()

            assertEquals(Resource.Status.SUCCESS, viewModel.networkRequestStatus.value)
        }

    @Test
    fun statusIsError_whenInitialNetworkCallReturnsError() =
        mainCoroutineRule.runBlocking {
            whenever(repository.refreshAllPosts()).thenReturn(Resource.Status.ERROR)

            mainCoroutineRule.testDispatcher.resumeDispatcher()

            assertEquals(Resource.Status.ERROR, viewModel.networkRequestStatus.value)
        }

    @Test
    fun additionalRequestToRepository_whenPostsAreRefreshed() =
        mainCoroutineRule.runBlocking {
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            viewModel.refreshData()

            verify(repository, times(2)).refreshAllPosts()
            verify(repository, times(1)).getAllPosts()
        }

    @Test
    fun statusIsUpdated_whenPostsAreRefreshed() =
        mainCoroutineRule.runBlocking {
            whenever(repository.refreshAllPosts()).thenReturn(Resource.Status.SUCCESS)

            mainCoroutineRule.testDispatcher.resumeDispatcher()

            viewModel.refreshData()

            verify(statusObserver, times(2)).onChanged(Resource.Status.LOADING)
            verify(statusObserver, times(2)).onChanged(Resource.Status.SUCCESS)
        }

    @Test
    fun postsLiveDataUpdates_whenRepositoryEmitsNewPostsValues() =
        mainCoroutineRule.runBlocking {
            mainCoroutineRule.testDispatcher.resumeDispatcher()

            verify(postsObserver, times(3)).onChanged(notNull())
        }
}