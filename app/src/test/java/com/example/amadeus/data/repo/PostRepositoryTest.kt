package com.example.amadeus.data.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.amadeus.MainCoroutineRule
import com.example.amadeus.data.local.PostEntity
import com.example.amadeus.data.local.PostsDao
import com.example.amadeus.data.remote.RemoteDataSource
import com.example.amadeus.runBlocking
import com.example.amadeus.util.network.Resource
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class PostRepositoryTest{
    private lateinit var repository: PostRepository
    private val remoteData: RemoteDataSource = mock()
    private val localData: PostsDao = mock()

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        repository = PostRepository(remoteData, localData)
    }


    @Test
    fun repositoryShouldEmitNewValues_whenLocalSourceEmitsNewValues() =
        mainCoroutineRule.runBlocking {
            val entity = PostEntity()
            val listOfOneEntity: List<PostEntity> = listOf(entity)
            val listOfMultipleEntities: List<PostEntity> = listOf(entity, entity)
            whenever(localData.getAllPosts()).thenReturn(
                flow {
                    emit(listOfOneEntity)
                    emit(listOfMultipleEntities)
                }
            )

            val smth = repository.getAllPosts()
            smth.collectIndexed { index, value ->
                if (index == 0) assert(value.size == listOfOneEntity.size)
                if (index == 1) assert(value.size == listOfMultipleEntities.size)
            }
        }

    @Test
    fun repositoryShouldEmitAllValues_whenItReceivesEmptyList() =
        mainCoroutineRule.runBlocking {
            val postEntity = PostEntity()
            val emptyList: List<PostEntity> = emptyList()
            val listOfMultipleEntities: List<PostEntity> = listOf(postEntity, postEntity)
            whenever(localData.getAllPosts()).thenReturn(
                flow {
                    emit(listOfMultipleEntities)
                    emit(emptyList)
                    emit(listOfMultipleEntities)
                }
            )

            val smth = repository.getAllPosts()
            smth.collectIndexed { index, value ->
                if (index == 0) assert(value.size == listOfMultipleEntities.size)
                if (index == 1) assert(value.size == emptyList.size)
                if (index == 2) assert(value.size == listOfMultipleEntities.size)
            }
        }

    @Test
    fun returnStatusSuccess_whenApiCallISSuccessFull() =
        mainCoroutineRule.runBlocking {
            whenever(remoteData.getAllPosts()).thenReturn(
                Resource.success(emptyList())
            )

            val networkResponse = repository.refreshAllPosts()
            assertEquals(networkResponse, Resource.Status.SUCCESS)
        }

    @Test
    fun returnStatusError_whenApiCallIsNotSuccessFull() =
        mainCoroutineRule.runBlocking {
            whenever(remoteData.getAllPosts()).thenReturn(
                Resource.error("", emptyList())
            )

            val networkResponse = repository.refreshAllPosts()
            assertEquals(networkResponse, Resource.Status.ERROR)
        }

    @Test
    fun writeOnceInDataBase_whenApiCallIsSuccessFull() =
        mainCoroutineRule.runBlocking {
            whenever(remoteData.getAllPosts()).thenReturn(
                Resource.success(emptyList())
            )

            repository.refreshAllPosts()
            verify(localData, times(1)).insertPosts(any())
        }

    @Test
    fun noInteractionWithDatabase_whenApiCallIsNotSuccessFull() =
        mainCoroutineRule.runBlocking {
            whenever(remoteData.getAllPosts()).thenReturn(
                Resource.error("" ,emptyList())
            )

            repository.refreshAllPosts()
            verify(localData, never()).insertPosts(any())
        }
}