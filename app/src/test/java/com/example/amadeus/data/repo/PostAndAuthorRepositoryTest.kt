package com.example.amadeus.data.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.amadeus.MainCoroutineRule
import com.example.amadeus.data.local.*
import com.example.amadeus.data.remote.PostDto
import com.example.amadeus.data.remote.RemoteDataSource
import com.example.amadeus.data.remote.UserDto
import com.example.amadeus.runBlocking
import com.example.amadeus.util.network.Resource
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.InOrder

@ExperimentalCoroutinesApi
class PostAndAuthorRepositoryTest {
    private lateinit var repository: PostAndAuthorRepository
    private val remoteData: RemoteDataSource = mock()
    private val postDao: PostsDao = mock()
    private val userDao: UserDao = mock()


    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun init() {
        repository = PostAndAuthorRepository(remoteData, postDao, userDao)
    }


    @Test
    fun repositoryEmitsNewValues_WhenDatabaseProvidesNewValues() =
        mainCoroutineRule.runBlocking {
            val post1 = PostWithAuthor(PostEntity(), UserEntity())
            val post2 = PostWithAuthor(PostEntity(id = 1), UserEntity())
            whenever(postDao.getPostWithAuthor(any())).thenReturn(
                flow {
                    emit(post1)
                    emit(post2)
                }
            )

            repository.requestInitialPostWithAuthorData(any()).collectIndexed { index, value ->
                if (index == 0) assert(value?.post?.id == 0)
                if (index == 1) assert(value?.post?.id == 1)
            }
        }

    @Test
    fun repositoryEmitsNullValue_WhenDatabaseReturnsNull() =
        mainCoroutineRule.runBlocking {
            val post = PostWithAuthor(PostEntity(), UserEntity())
            whenever(postDao.getPostWithAuthor(any())).thenReturn(
                flow {
                    emit(null)
                    emit(post)
                }
            )

            repository.requestInitialPostWithAuthorData(any()).collectIndexed { index, value ->
                if (index == 0) assertNull(value)
                if (index == 1) assertNotNull(value)
            }
        }

    @Test
    fun errorStatusReturned_whenDatabaseReturnsNullPostAuthorId() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(any())).thenReturn(null)

            val reponse = repository.refreshPostDetails(any())
            assertEquals(Resource.Status.ERROR, reponse)
        }

    @Test
    fun successStatusReturned_whenAllNetworkCallsAreSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.success(UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.success(PostDto()))

            val reponse = repository.refreshPostDetails(any())
            assertEquals(Resource.Status.SUCCESS, reponse)
        }

    @Test
    fun returnErrorStatus_whenNetworkCallForUserIsNotSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.error("", UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.success(PostDto()))

            val reponse = repository.refreshPostDetails(any())
            assertEquals(Resource.Status.ERROR, reponse)
        }

    @Test
    fun returnErrorStatus_whenNetworkCallForPostIsNotSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.success(UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.error("", PostDto()))

            val reponse = repository.refreshPostDetails(any())
            assertEquals(Resource.Status.ERROR, reponse)
        }

    @Test
    fun successStatusReturned_whenAllNetworkCallsAreNotSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.error("", UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.error("", PostDto()))

            val reponse = repository.refreshPostDetails(any())
            assertEquals(Resource.Status.ERROR, reponse)
        }

    @Test
    fun noEntryToDatabase_whenAllNetworkCallsAreNotSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.error("", UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.error("", PostDto()))

            repository.refreshPostDetails(any())

            verify(postDao, never()).insertPost(any())
            verify(userDao, never()).insertUser(any())
        }

    @Test
    fun updateLocalUserData_whenNetworkCallForUserIsSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.success(UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.error("", PostDto()))

            repository.refreshPostDetails(any())

            verify(userDao, times(1)).insertUser(any())
            verify(postDao, never()).insertPost(any())
        }

    @Test
    fun updateLocalPostData_whenNetworkCallForUserIsSuccessful() =
        mainCoroutineRule.runBlocking {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.error("", UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.success(PostDto()))

            repository.refreshPostDetails(any())

            verify(userDao, never()).insertUser(any())
            verify(postDao, times(1)).insertPost(any())
        }

    @Test
    fun updateLocalDataOnce_whenAllNetworkCallsAreSuccessful() =
        mainCoroutineRule.testDispatcher.runBlockingTest {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.success(UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.success(PostDto()))

            repository.refreshPostDetails(any())

            verify(userDao, times(1)).insertUser(any())
            verify(postDao, times(1)).insertPost(any())
        }

    @Test
    fun queryUserFirstThanPost_whenRefreshingPostDetails() =
        mainCoroutineRule.testDispatcher.runBlockingTest {
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)
            whenever(remoteData.getTheUser(anyInt())).thenReturn(Resource.success(UserDto()))
            whenever(remoteData.getThePost(anyInt())).thenReturn(Resource.success(PostDto()))
            whenever(postDao.getPostAuthorId(anyInt())).thenReturn(0)

            repository.refreshPostDetails(any())

            val orderVerifier : InOrder = inOrder(remoteData)
            orderVerifier.verify(remoteData).getTheUser(any())
            orderVerifier.verify(remoteData).getThePost(any())
        }
}