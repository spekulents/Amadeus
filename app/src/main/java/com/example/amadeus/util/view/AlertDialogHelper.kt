package com.example.amadeus.util.view

import android.app.AlertDialog
import android.content.Context
import com.example.amadeus.R

class AlertDialogHelper() {
    fun launchSimpleDialog(context: Context, onRetryRun :() -> Unit){
        val builder: AlertDialog.Builder =  AlertDialog.Builder(context)
        builder.setMessage(R.string.dialog_message)
            ?.setPositiveButton(R.string.retry) { dialog, id -> onRetryRun() }
            ?.setNegativeButton(R.string.cancel) { dialog, id -> }
            ?.create()?.show()
    }
}