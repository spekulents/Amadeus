package com.example.amadeus.util.view

import androidx.lifecycle.ViewModel
import com.example.amadeus.util.network.Resource

open class NetworkRequestStatusViewModel() : ViewModel() {
    val networkRequestStatus: SingleLiveEvent<Resource.Status>
        get() = _networkRequestStatus
    protected  val _networkRequestStatus: SingleLiveEvent<Resource.Status> = SingleLiveEvent()
}