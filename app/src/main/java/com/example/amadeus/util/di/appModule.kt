package com.example.amadeus.util.di

import android.content.Context
import com.example.amadeus.data.repo.PostAndAuthorRepository
import com.example.amadeus.data.repo.PostRepository
import com.example.amadeus.data.local.AppDatabase
import com.example.amadeus.data.local.UserDao
import com.example.amadeus.data.local.PostsDao
import com.example.amadeus.data.remote.PostService
import com.example.amadeus.data.remote.RemoteDataSource
import com.example.amadeus.util.view.AlertDialogHelper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun providePostRepository(remoteDataSource: RemoteDataSource, localDataSource: PostsDao) : PostRepository = PostRepository(remoteDataSource, localDataSource)

    @Singleton
    @Provides
    fun providePostAndAuthorRepository(
        remoteDataSource: RemoteDataSource,
        postDao: PostsDao,
        userDao: UserDao
    ): PostAndAuthorRepository = PostAndAuthorRepository(remoteDataSource,postDao, userDao)

    @Singleton
    @Provides
    fun provideRemoteDataSource(postService: PostService) :  RemoteDataSource = RemoteDataSource(postService)

    @Provides
    fun providePostService(retrofit: Retrofit): PostService = retrofit.create(PostService::class.java)

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()


    @Singleton
    @Provides
    fun providePostDao(db: AppDatabase) : PostsDao = db.postDao()

    @Singleton
    @Provides
    fun provideUserDao(db: AppDatabase) : UserDao = db.postWithAuthorDao()

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Provides
    fun provideCoroutines(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    fun provideAlertHelper(): AlertDialogHelper = AlertDialogHelper()
}