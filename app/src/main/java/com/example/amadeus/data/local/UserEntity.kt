package com.example.amadeus.data.local

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.amadeus.ui.main.postdetails.PostDetailsPresentation

@Entity(tableName = "users")
data class UserEntity(
    @PrimaryKey
    val userId: Int = 0,
    val userName: String = "",
    val imageUrl: String = ""
)

data class PostWithAuthor(
    @Embedded val post: PostEntity,
    @Embedded val user: UserEntity?
)

fun PostWithAuthor.asUiModel(): PostDetailsPresentation {
    return PostDetailsPresentation(
        postId = this.post.id,
        title = this.post.title,
        textBody = this.post.body,
        userName = this.user?.userName ?: "",
        imageUrl = this.user?.imageUrl ?: ""
    )
}