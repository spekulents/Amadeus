package com.example.amadeus.data.repo

import com.example.amadeus.data.local.PostEntity
import com.example.amadeus.data.local.PostsDao
import com.example.amadeus.data.remote.RemoteDataSource
import com.example.amadeus.util.network.Resource
import com.example.amadeus.data.remote.asDatabaseEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: PostsDao
) {
    fun getAllPosts(): Flow<List<PostEntity>> = flow {
        emitAll(localDataSource.getAllPosts())
    }

    suspend fun refreshAllPosts(): Resource.Status {
        val networkResponse = remoteDataSource.getAllPosts()
        return if (networkResponse.status == Resource.Status.SUCCESS) {
            localDataSource.insertPosts(networkResponse.data!!.asDatabaseEntity())
            Resource.Status.SUCCESS
        } else {
            Resource.Status.ERROR
        }
    }
}
