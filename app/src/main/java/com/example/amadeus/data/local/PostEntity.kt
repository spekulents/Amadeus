package com.example.amadeus.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.amadeus.ui.main.postdetails.PostDetailsPresentation
import com.example.amadeus.ui.main.posts.PostPresentation

@Entity(tableName = "posts")
data class PostEntity(
    @PrimaryKey
    val id : Int = 0,
    @ColumnInfo(name = "author_id")
    val userId : Int = 0,
    val title : String = "",
    val body : String = ""
)

fun List<PostEntity>.asUiModel(): List<PostPresentation> {
        return this.map {
            PostPresentation(
                postId = it.id,
                userId = it.userId,
                title = it.title
            )
        }
}