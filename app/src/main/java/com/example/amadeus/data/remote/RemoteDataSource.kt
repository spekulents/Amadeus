package com.example.amadeus.data.remote

import com.example.amadeus.util.network.NetworkUtil.getResult
import com.example.amadeus.util.network.Resource
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val service : PostService
) {
    suspend fun getAllPosts() : Resource<List<PostDto>> {
        return getResult {service.getAllPosts()}
    }

    suspend fun getThePost(postId : Int) : Resource<PostDto> {
        return getResult {service.getThePost(postId)}
    }

    suspend fun getTheUser(userId : Int) : Resource<UserDto> {
        return getResult {service.getTheUser(userId)}
    }
}
