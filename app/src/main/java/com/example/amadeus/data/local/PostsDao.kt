package com.example.amadeus.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface PostsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPosts(posts: List<PostEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPost(posts: PostEntity)

    @Query("SELECT * FROM posts")
    fun getAllPosts(): Flow<List<PostEntity>>

    @Query("SELECT * FROM posts WHERE id = :postId")
    fun getPost(postId: Int): Flow<PostEntity>

    @Query("SELECT author_id FROM posts WHERE id = :postId")
    fun getPostAuthorId(postId: Int): Int?

    @Query(
        """
    SELECT * FROM posts
    LEFT JOIN users ON posts.author_id = users.userId
    WHERE posts.id =:postId"""
    )
    fun getPostWithAuthor(postId: Int): Flow<PostWithAuthor?>
}
