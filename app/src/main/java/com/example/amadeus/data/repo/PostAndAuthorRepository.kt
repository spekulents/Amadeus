package com.example.amadeus.data.repo

import com.example.amadeus.data.local.PostWithAuthor
import com.example.amadeus.data.local.UserDao
import com.example.amadeus.data.local.PostsDao
import com.example.amadeus.data.remote.RemoteDataSource
import com.example.amadeus.data.remote.asDatabaseEntity
import com.example.amadeus.util.network.Resource
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class PostAndAuthorRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val postDao: PostsDao,
    private val authorDao: UserDao
) {
    fun requestInitialPostWithAuthorData(postId: Int): Flow<PostWithAuthor?> = flow {
        emitAll(postDao.getPostWithAuthor(postId))
    }

    suspend fun refreshPostDetails(postId: Int): Resource.Status {
        val postAuthorId = postDao.getPostAuthorId(postId)
        if (postAuthorId != null) {
            val userRequestStatus = updateUserFromRemote(postAuthorId)
            val postRequestStatus = updatePostFromRemote(postId)
            return if (postRequestStatus == Resource.Status.SUCCESS && userRequestStatus == Resource.Status.SUCCESS) {
                Resource.Status.SUCCESS
            } else {
                Resource.Status.ERROR
            }
        }else{
            return Resource.Status.ERROR
        }
    }

    private suspend fun updateUserFromRemote(userId: Int): Resource.Status {
        val remoteUserData = remoteDataSource.getTheUser(userId)
        return if (remoteUserData.status == Resource.Status.SUCCESS && remoteUserData.data != null) {
            authorDao.insertUser(remoteUserData.data.asDatabaseEntity())
            remoteUserData.status
        } else {
            remoteUserData.status
        }
    }

    private suspend fun updatePostFromRemote(postId: Int): Resource.Status {
        val remotePostData = remoteDataSource.getThePost(postId)
        return if (remotePostData.status == Resource.Status.SUCCESS && remotePostData.data != null) {
            postDao.insertPost(remotePostData.data.asDatabaseEntity())
            remotePostData.status
        } else {
            remotePostData.status
        }
    }
}
