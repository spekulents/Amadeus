package com.example.amadeus.data.remote

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PostService {
    @GET("posts")
    suspend fun getAllPosts() : Response<List<PostDto>>

    @GET("posts/{post_id}")
    suspend fun getThePost(@Path("post_id") postId: Int) : Response<PostDto>

    @GET("users/{user_id}")
    suspend fun getTheUser(@Path("user_id") userId : Int) : Response<UserDto>
}
