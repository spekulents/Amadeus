package com.example.amadeus.ui.main.postdetails

data class PostDetailsPresentation(
    val postId :  Int,
    val title: String,
    val textBody : String,
    val imageUrl : String,
    val userName : String
)