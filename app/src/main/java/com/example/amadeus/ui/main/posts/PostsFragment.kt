package com.example.amadeus.ui.main.posts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.amadeus.databinding.FragmentPostsBinding
import com.example.amadeus.util.network.Resource
import com.example.amadeus.util.view.AlertDialogHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {

    @Inject
    lateinit var alertHelper: AlertDialogHelper

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: FragmentPostsBinding
    private lateinit var adapter: ListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPostsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpObservers()
        setUpRecyclerView()
        setUpPullToRefresh()
    }

    private fun setUpPullToRefresh() {
        binding.swipeContainer.setOnRefreshListener {
            viewModel.refreshData()
        }
    }

    private fun setUpRecyclerView() {
        adapter = ListAdapter(viewModel.posts.value ?: emptyList())
        binding.listView.layoutManager = LinearLayoutManager(context)
        binding.listView.adapter = adapter

    }

    private fun setUpObservers() {
        viewModel.posts.observe(viewLifecycleOwner, Observer {
            it?.let { adapter.refreshListItems(it) }
        })

        viewModel.networkRequestStatus.observe(viewLifecycleOwner, {
            when (it) {
                Resource.Status.LOADING -> binding.swipeContainer.isRefreshing = true
                Resource.Status.SUCCESS -> binding.swipeContainer.isRefreshing = false
                else -> {
                    binding.swipeContainer.isRefreshing = false
                    alertHelper.launchSimpleDialog(requireActivity()) { viewModel.refreshData() }
                }
            }
        })
    }
}