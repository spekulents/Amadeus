package com.example.amadeus.ui.main.posts

import androidx.lifecycle.*
import com.example.amadeus.data.repo.PostRepository
import com.example.amadeus.data.local.asUiModel
import com.example.amadeus.util.view.NetworkRequestStatusViewModel
import com.example.amadeus.util.network.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: PostRepository,
    private val dispatcherIO: CoroutineDispatcher
) : NetworkRequestStatusViewModel() {
    val posts: LiveData<List<PostPresentation>?>
        get() = _posts

    private val _posts: LiveData<List<PostPresentation>?> = liveData {
        val postsFromRepository = repository.getAllPosts().map { it.asUiModel() }
        emitSource(postsFromRepository.asLiveData(viewModelScope.coroutineContext))
        refreshData()
    }

    fun refreshData() {
        _networkRequestStatus.postValue(Resource.Status.LOADING)
        viewModelScope.launch(dispatcherIO) {
            _networkRequestStatus.postValue(repository.refreshAllPosts())
        }
    }
}