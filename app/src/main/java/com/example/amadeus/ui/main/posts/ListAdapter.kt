package com.example.amadeus.ui.main.posts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.amadeus.databinding.LineItemPostBinding

class ListAdapter(
    private var list: List<PostPresentation>
) : RecyclerView.Adapter<ListAdapter.IssuesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssuesViewHolder {
        val rowView =
            LineItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return IssuesViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: IssuesViewHolder, position: Int) {
        holder.binding.itemTitle.text = list[position].title


        holder.binding.root.setOnClickListener {
            val navigationAction =
                MainFragmentDirections.actionPostListFragmentToPostDetailsFragment(
                    list.get(position).postId
                )
            holder.binding.root.findNavController().navigate(navigationAction)
        }
    }

    override fun getItemCount(): Int = list.size
    fun refreshListItems(newList :List<PostPresentation>) {
        list = newList
        notifyDataSetChanged()
    }

    inner class IssuesViewHolder(val binding: LineItemPostBinding) : RecyclerView.ViewHolder(binding.root)
}

