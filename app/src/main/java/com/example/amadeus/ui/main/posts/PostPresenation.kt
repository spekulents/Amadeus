package com.example.amadeus.ui.main.posts

data class PostPresentation(
    val postId :  Int,
    val userId : Int,
    val title: String
) {
}