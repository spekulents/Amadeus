package com.example.amadeus.ui.main.postdetails

import androidx.lifecycle.*
import com.example.amadeus.data.local.asUiModel
import com.example.amadeus.data.repo.PostAndAuthorRepository
import com.example.amadeus.util.view.NetworkRequestStatusViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostDetailsViewModel @Inject constructor(
    private val repository: PostAndAuthorRepository,
    private val dispatcherIO: CoroutineDispatcher
) : NetworkRequestStatusViewModel() {
    private var _postId: Int? = null

    val postData: LiveData<PostDetailsPresentation?>
        get() = _post

    private val _post: LiveData<PostDetailsPresentation?> = liveData {
        _postId?.let {
            emitSource(repository.requestInitialPostWithAuthorData(it)
                .asLiveData(viewModelScope.coroutineContext).map { it?.asUiModel() })
            refreshData()
        }
    }

    fun setPostId(postId: Int) {
        _postId = postId
    }

    fun refreshData() {
        viewModelScope.launch(dispatcherIO) {
            _postId?.let {
                val networkResponse = repository.refreshPostDetails(it)
                _networkRequestStatus.postValue(networkResponse)
            }
        }
    }
}