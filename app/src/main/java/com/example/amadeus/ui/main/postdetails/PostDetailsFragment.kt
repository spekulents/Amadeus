package com.example.amadeus.ui.main.postdetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.amadeus.databinding.FragmentPostDetailsBinding
import com.example.amadeus.util.network.Resource
import com.example.amadeus.util.view.AlertDialogHelper
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PostDetailsFragment : Fragment() {

    @Inject
    lateinit var alertHelper : AlertDialogHelper

    private val viewModel: PostDetailsViewModel by viewModels()
    private lateinit var binding: FragmentPostDetailsBinding
    val postDetailsArg: PostDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPostDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val postId = postDetailsArg.postId
        viewModel.setPostId(postId)
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.postData.observe(viewLifecycleOwner, Observer {
            binding.post = it
        })

        viewModel.networkRequestStatus.observe(viewLifecycleOwner, Observer {
            if(it == Resource.Status.ERROR){
                alertHelper.launchSimpleDialog(requireActivity()) { viewModel.refreshData() }
            }
        })
    }
}